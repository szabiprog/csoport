package hu.braininghub.bh04.csf.service;

import hu.braininghub.bh04.csf.model.dto.MeasurementDTO;
import javax.ejb.Local;

@Local
public interface MeasurementService {
    
 public Long persist(MeasurementDTO object);

 public MeasurementDTO getObjectById(Long id);

  public void update(MeasurementDTO object);

  public void delete(MeasurementDTO object);
    
}
