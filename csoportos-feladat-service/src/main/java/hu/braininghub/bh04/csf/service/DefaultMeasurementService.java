package hu.braininghub.bh04.csf.service;

import hu.braininghub.bh04.csf.dao.MeasurementDao;
import hu.braininghub.bh04.csf.model.dto.MeasurementDTO;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class DefaultMeasurementService implements MeasurementService {

    @Inject
    private MeasurementDao measurementDao;

    @Override
    public Long persist(MeasurementDTO object) {
        
        LocalDateTime localDate = LocalDateTime.now();//For reference
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy.MM.dd.:HH.mm.ss.");
        String formattedString = localDate.format(formatter);
        object.setMeasurementTimestamp(formattedString);
        
        
        return measurementDao.persist(object);
    }

    @Override
    public MeasurementDTO getObjectById(Long id) {
        return measurementDao.getObjectById(id);
    }

    @Override
    public void update(MeasurementDTO object) {
        measurementDao.update(object);
    }

    @Override
    public void delete(MeasurementDTO object) {
        measurementDao.delete(object.getMeasurementId());
    }

}
