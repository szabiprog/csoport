package hu.braininghub.bh04.csf.service;

import hu.braininghub.bh04.csf.model.dto.SensorDTO;
import javax.ejb.Local;

@Local
public interface SensorService {

  void persist(SensorDTO object);

  SensorDTO getObjectById(String id);

  void update(SensorDTO object);

  void delete(SensorDTO object);

}
