package hu.braininghub.bh04.csf.service;

import hu.braininghub.bh04.csf.dao.SensorDao;
import hu.braininghub.bh04.csf.model.dto.SensorDTO;
import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class DefaultSensorService implements SensorService {



  @Inject
  private SensorDao sensorDao;

  @Override
  public void persist(SensorDTO object) {
    sensorDao.persist(object);
  }

  @Override
  public SensorDTO getObjectById(String id) {
    return sensorDao.getObjectById(id);
  }

  @Override
  public void update(SensorDTO object) {
    sensorDao.update(object);
  }

  @Override
  public void delete(SensorDTO object) {
    sensorDao.delete(object.getSensorID());
  }

}
