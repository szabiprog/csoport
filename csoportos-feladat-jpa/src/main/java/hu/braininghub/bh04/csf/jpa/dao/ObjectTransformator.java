package hu.braininghub.bh04.csf.jpa.dao;


import hu.braininghub.bh04.csf.model.Measurement;
import hu.braininghub.bh04.csf.model.Sensor;
import hu.braininghub.bh04.csf.model.dto.MeasurementDTO;
import hu.braininghub.bh04.csf.model.dto.SensorDTO;



public class ObjectTransformator {

    private ObjectTransformator() {
    }

    public static SensorDTO sensorToSensorDTO(Sensor sensor) {
        SensorDTO sensorDTO = new SensorDTO();
        sensorDTO.setActive(sensor.isActive());
        sensorDTO.setSensorID(sensor.getSensorID());
        sensorDTO.setLatitude(sensor.getLatitude());
        sensorDTO.setLongitude(sensor.getLongitude());
        sensorDTO.setSensorName(sensor.getSensorName());
        
        return sensorDTO;
    }

    public static Sensor sensorDTOtoSensor(SensorDTO sensorDTO) {
        Sensor sensor = new Sensor();
        sensor.setActive(sensorDTO.isActive());
        sensor.setSensorID(sensorDTO.getSensorID());
        sensor.setLatitude(sensorDTO.getLatitude());
        sensor.setLongitude(sensorDTO.getLongitude());
        sensor.setSensorName(sensorDTO.getSensorName());
        return sensor;
    }

    public static MeasurementDTO measurementToMeasurementDTO(Measurement measurement) {
        MeasurementDTO measurementDTO = new MeasurementDTO();
        measurementDTO.setActive(measurement.isActive());
        measurementDTO.setMeasurementId(measurement.getMeasurementId());
        measurementDTO.setMeasurementTimestamp(measurement.getMeasurementTimestamp());
        measurementDTO.setValue(measurement.getValue());
        if (measurement.getSensor()!=null ) measurementDTO.setSensor(sensorToSensorDTO(measurement.getSensor()));
         else measurementDTO.setSensor(null);
        return measurementDTO;
    }

    public static Measurement measurementDTOToMeasurement(MeasurementDTO measurementDTO) {
        Measurement measurement = new Measurement();
        measurement.setActive(measurementDTO.isActive());
        measurement.setMeasurementId(measurementDTO.getMeasurementId());
        measurement.setMeasurementTimestamp(measurementDTO.getMeasurementTimestamp());
        measurement.setValue(measurementDTO.getValue());
        if (measurementDTO.getSensor()!=null ) measurement.setSensor(sensorDTOtoSensor(measurementDTO.getSensor()));
         else measurement.setSensor(null);
        return measurement;
    }

}
