package hu.braininghub.bh04.csf.jpa.dao;

import hu.braininghub.bh04.csf.model.dto.MeasurementDTO;
import hu.braininghub.bh04.csf.model.Measurement;
import javax.enterprise.context.Dependent;
import hu.braininghub.bh04.csf.dao.MeasurementDao;
import hu.braininghub.bh04.csf.model.Sensor;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Dependent
public class JpaMeasurementDao implements MeasurementDao {

   
    
    @PersistenceContext(name = "bfoPU")
    protected EntityManager em;

    @Override
    public Long persist(MeasurementDTO objectDTO) {
        objectDTO.setActive(true);
        objectDTO.setMeasurementId(0l);
        Measurement o = ObjectTransformator.measurementDTOToMeasurement(objectDTO);
        Sensor s =em.find(Sensor.class, objectDTO.getSensor().getSensorID());
        o.setSensor(s);
        em.persist(o);
        em.flush();
        return o.getMeasurementId();
    }

    @Override
    public void update(MeasurementDTO objectDTO) {
        Measurement m = em.find(Measurement.class, ObjectTransformator.measurementDTOToMeasurement(objectDTO).getMeasurementId());
        em.persist(m);
    }

    @Override
    public MeasurementDTO getObjectById(Long id) {

        Measurement m = null;

        try {

            m = em.find(Measurement.class, 1l);
        } catch (Exception e) {
        }

        if (m == null) {
            return null;
        }
        return ObjectTransformator.measurementToMeasurementDTO(m);
    }

    @Override
    public void delete(Long id) {
        Measurement m = em.find(Measurement.class, id);
        m.setActive(false);
        em.persist(m);
    }

}
