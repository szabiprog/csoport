package hu.braininghub.bh04.csf.model;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "measurement")
public class Measurement extends  BusinessObject{

 
  private boolean active;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long measurementId;

  private Sensor sensor;

  private double value;

  private String measurementTimestamp;

  public Measurement() {
  }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Long getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(Long measurementId) {
        this.measurementId = measurementId;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getMeasurementTimestamp() {
        return measurementTimestamp;
    }

    public void setMeasurementTimestamp(String measurementTimestamp) {
        this.measurementTimestamp = measurementTimestamp;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.active ? 1 : 0);
        hash = 17 * hash + Objects.hashCode(this.measurementId);
        hash = 17 * hash + Objects.hashCode(this.sensor);
        hash = 17 * hash + (int) (Double.doubleToLongBits(this.value) ^ (Double.doubleToLongBits(this.value) >>> 32));
        hash = 17 * hash + Objects.hashCode(this.measurementTimestamp);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Measurement other = (Measurement) obj;
        if (this.active != other.active) {
            return false;
        }
        if (Double.doubleToLongBits(this.value) != Double.doubleToLongBits(other.value)) {
            return false;
        }
        if (!Objects.equals(this.measurementTimestamp, other.measurementTimestamp)) {
            return false;
        }
        if (!Objects.equals(this.measurementId, other.measurementId)) {
            return false;
        }
        if (!Objects.equals(this.sensor, other.sensor)) {
            return false;
        }
        return true;
    }

 

}
