package hu.braininghub.bh04.csf.jpa.dao;

import hu.braininghub.bh04.csf.dao.SensorDao;
import hu.braininghub.bh04.csf.model.BusinessObject;
import hu.braininghub.bh04.csf.model.dto.SensorDTO;
import hu.braininghub.bh04.csf.model.Sensor;
import javax.enterprise.context.Dependent;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Dependent
public class JpaSensorDao implements SensorDao {

    @PersistenceContext(name = "bfoPU")
    protected EntityManager em;

    @Override
    public void persist(SensorDTO object) {
        object.setActive(true);
        Sensor s = ObjectTransformator.sensorDTOtoSensor(object);
        em.persist(s);
        em.flush();
    }

    @Override
    public void update(SensorDTO objectDTO) {
        Sensor s = em.find(Sensor.class, ObjectTransformator.sensorDTOtoSensor(objectDTO).getSensorID());
        s.setLatitude(objectDTO.getLatitude());
        s.setLongitude(objectDTO.getLatitude());
        s.setSensorName(objectDTO.getSensorName());
        em.flush();
    }

    @Override
    public SensorDTO getObjectById(String id) {
        Sensor s = null;
        try {
            s = em.find(Sensor.class, id);
        } catch (Exception e) {
        }
        if (s == null || !s.isActive() ) {
            return null;
        }

        return ObjectTransformator.sensorToSensorDTO(s);
    }

    @Override
    public void delete(String id) {
     Sensor s  = em.find(Sensor.class, id);
            s.setActive(false);
            em.flush();
      }

}
