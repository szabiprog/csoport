package hu.braininghub.bh04.csf.dao;

import hu.braininghub.bh04.csf.model.dto.SensorDTO;

public interface SensorDao {

  void persist(SensorDTO object);

  SensorDTO getObjectById(String id);

  void update(SensorDTO object);

  void delete(String id);
}
