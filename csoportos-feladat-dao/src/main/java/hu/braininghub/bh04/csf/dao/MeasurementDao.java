package hu.braininghub.bh04.csf.dao;

import hu.braininghub.bh04.csf.model.dto.MeasurementDTO;

public interface MeasurementDao {

  Long persist(MeasurementDTO object);

  MeasurementDTO getObjectById(Long id);

  void update(MeasurementDTO object);

  void delete(Long id);
}
