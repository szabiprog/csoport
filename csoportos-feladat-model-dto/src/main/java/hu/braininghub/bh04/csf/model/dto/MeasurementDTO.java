package hu.braininghub.bh04.csf.model.dto;

import java.time.LocalDateTime;
import java.util.Objects;


public class MeasurementDTO extends BaseDTO {

 private Long measurementId;

 private SensorDTO sensor;

 private double value;
 
 private String measurementTimestamp;


 public MeasurementDTO() {
 }

    public Long getMeasurementId() {
        return measurementId;
    }

    public void setMeasurementId(Long measurementId) {
        this.measurementId = measurementId;
    }

    public SensorDTO getSensor() {
        return sensor;
    }

    public void setSensor(SensorDTO sensor) {
        this.sensor = sensor;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getMeasurementTimestamp() {
        return measurementTimestamp;
    }

    public void setMeasurementTimestamp(String measurementTimestamp) {
        this.measurementTimestamp = measurementTimestamp;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.measurementId);
        hash = 79 * hash + Objects.hashCode(this.sensor);
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.value) ^ (Double.doubleToLongBits(this.value) >>> 32));
        hash = 79 * hash + Objects.hashCode(this.measurementTimestamp);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MeasurementDTO other = (MeasurementDTO) obj;
        if (Double.doubleToLongBits(this.value) != Double.doubleToLongBits(other.value)) {
            return false;
        }
        if (!Objects.equals(this.measurementTimestamp, other.measurementTimestamp)) {
            return false;
        }
        if (!Objects.equals(this.measurementId, other.measurementId)) {
            return false;
        }
        if (!Objects.equals(this.sensor, other.sensor)) {
            return false;
        }
        return true;
    }

}
