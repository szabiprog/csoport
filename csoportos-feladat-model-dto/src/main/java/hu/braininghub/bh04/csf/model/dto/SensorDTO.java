package hu.braininghub.bh04.csf.model.dto;

import java.util.Objects;


public class SensorDTO extends BaseDTO{
 
 private String sensorID;
 
 private String sensorName;
 
 private double latitude;
 
 private double longitude;

    public String getSensorID() {
        return sensorID;
    }

    public void setSensorID(String sensorID) {
        this.sensorID = sensorID;
    }

    public String getSensorName() {
        return sensorName;
    }

    public void setSensorName(String sensorName) {
        this.sensorName = sensorName;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.sensorID);
        hash = 31 * hash + Objects.hashCode(this.sensorName);
        hash = 31 * hash + (int) (Double.doubleToLongBits(this.latitude) ^ (Double.doubleToLongBits(this.latitude) >>> 32));
        hash = 31 * hash + (int) (Double.doubleToLongBits(this.longitude) ^ (Double.doubleToLongBits(this.longitude) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SensorDTO other = (SensorDTO) obj;
        if (Double.doubleToLongBits(this.latitude) != Double.doubleToLongBits(other.latitude)) {
            return false;
        }
        if (Double.doubleToLongBits(this.longitude) != Double.doubleToLongBits(other.longitude)) {
            return false;
        }
        if (!Objects.equals(this.sensorID, other.sensorID)) {
            return false;
        }
        if (!Objects.equals(this.sensorName, other.sensorName)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "SensorDTO{" + "sensorID=" + sensorID + ", sensorName=" + sensorName + ", latitude=" + latitude + ", longitude=" + longitude + '}';
    }

 
 
 
 
}
