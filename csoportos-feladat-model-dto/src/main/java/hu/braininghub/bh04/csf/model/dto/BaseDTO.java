package hu.braininghub.bh04.csf.model.dto;

import java.io.Serializable;

public abstract class BaseDTO implements Serializable {

  static final long serialVersionUID = 1L;
  protected boolean active;

  public BaseDTO() {
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 53 * hash + (this.active ? 1 : 0);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final BaseDTO other = (BaseDTO) obj;
    if (this.active != other.active) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "BaseDTO{" + "active=" + active + '}';
  }

}
