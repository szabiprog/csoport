package hu.braininghub.bh04.csf.web;

import hu.braininghub.bh04.csf.model.dto.MeasurementDTO;
import hu.braininghub.bh04.csf.service.MeasurementService;
import java.net.URI;
import java.net.URISyntaxException;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;

@Path("/measurement")
public class MeasurementRestService {

    @EJB
    private MeasurementService measurementService;

    @Context
    private UriInfo uriInfo;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMeasurementById(@PathParam("id") Long measurementId) {

        MeasurementDTO measurement = null;

        measurement = measurementService.getObjectById(measurementId);
        if (measurement == null) {
            return Response.status(Response.Status.NOT_FOUND).
                    header("Entity ->  is not found: ", "" + measurementId)
                    .build();
        }

        return Response.status(Response.Status.OK).entity(measurement).build();
    }

//    @PUT
//    @Path("/")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response addMeasurement(MeasurementDTO mesurementDTO) throws URISyntaxException {
//        Long id = measurementService.persist(mesurementDTO);
//        URI location = new URI(uriInfo.getRequestUri().toString() +"/"+ id);
//        return Response.status(Response.Status.CREATED).location(location).build();
//    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response delete(@PathParam("id") Long measumentId) throws URISyntaxException {
        MeasurementDTO mesurement = null;

        try {
            mesurement = measurementService.getObjectById(measumentId);
        } catch (Exception ex) {
            return Response.status(Response.Status.NOT_FOUND).
                    header("message", ex.getMessage() + "Entity -> is not found: " + measumentId)
                    .build();
        }
        measurementService.delete(mesurement);
        URI location = new URI(uriInfo.getRequestUri().toString());
        return Response.status(Response.Status.OK).location(location).build();
    }

}
