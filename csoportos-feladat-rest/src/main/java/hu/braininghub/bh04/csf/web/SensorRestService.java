package hu.braininghub.bh04.csf.web;

import hu.braininghub.bh04.csf.model.dto.MeasurementDTO;
import hu.braininghub.bh04.csf.model.dto.SensorDTO;
import hu.braininghub.bh04.csf.service.MeasurementService;
import hu.braininghub.bh04.csf.service.SensorService;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.core.Response.Status;

@Path("/sensor")
public class SensorRestService {

    @EJB
    private SensorService sensorService;
    @EJB
    private MeasurementService measurementService;
    @Context
    private UriInfo uriInfo;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSensors() {
        List<SensorDTO> sensor = null;//= userService.getAll();
        return Response.status(Response.Status.OK).entity(sensor).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSensorByID(@PathParam("id") String id) {

        SensorDTO c = null;

        try {
            c = sensorService.getObjectById(id);
        } catch (Exception ex) {
            return Response.status(Status.NOT_FOUND).header("Origin message :",
                    ex.getMessage() + "Entity with the specified name is not found: " + id)
                    .build();
        }

        return Response.status(Response.Status.OK).entity(c).build();
    }

    @PUT
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addSensor(SensorDTO sensorDTO) throws URISyntaxException {
        sensorService.persist(sensorDTO);
        URI location = new URI(uriInfo.getRequestUri().toString() + "/" + sensorDTO.getSensorID());
        return Response.status(Response.Status.CREATED).location(location).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addMeasurement(@PathParam("id") String sensorId, MeasurementDTO mesurementDTO) throws URISyntaxException {

        SensorDTO c = null;

        try {
            c = sensorService.getObjectById(sensorId);
        } catch (Exception ex) {
            return Response.status(Status.NOT_FOUND).header("Origin message :",
                    ex.getMessage() + "Entity with the specified name is not found: " + sensorId)
                    .build();
        }
        
        mesurementDTO.setSensor(c);
        
        Long id = measurementService.persist(mesurementDTO);
        URI location = new URI(uriInfo.getRequestUri().toString() + "/" + id);
        return Response.status(Response.Status.CREATED).location(location).build();
    }

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response modifySensorById(SensorDTO sensor) throws URISyntaxException {

        SensorDTO c = null;
        try {
            c = sensorService.getObjectById(sensor.getSensorID());
        } catch (Exception ex) {
            return Response.status(Status.NOT_FOUND).
                    header("Origin message :", ex.getMessage()
                            + "OR -->  Entity  is not found: "
                            + sensor.getSensorID()).build();
        }
        sensorService.update(sensor);

        URI location = new URI(uriInfo.getRequestUri().toString());
        return Response.status(Response.Status.CREATED).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSensor(@PathParam("id") String id) throws URISyntaxException {

        SensorDTO c = null;

        try {
            c = sensorService.getObjectById(id);
        } catch (Exception ex) {
            return Response.status(Status.NOT_FOUND).header("Origin message :",
                    ex.getMessage() + "OR Entity with the specified id not found: " + id).build();
        }

        sensorService.delete(c);
        URI location = new URI(uriInfo.getRequestUri().toString());
        return Response.status(Response.Status.OK).build();
    }

}
